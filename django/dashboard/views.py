from django.shortcuts import render
from django.views.generic import TemplateView

# Create your views here.


class DashboardView(TemplateView):
    template_name = 'pages/dashboard.html'
    page_title = 'Dashboard'

    def get(self, request):
        context = {
            'title': self.page_title
        }

        return render(request, self.template_name, context)