from django.shortcuts import render
from django.views.generic import TemplateView

from tekyy.views import TekyyView

# Create your views here.


class AccountsListView(TemplateView):
    template_name = 'pages/accounts/index.html'
    page_title = 'Accounts'
    breadcrumbs = [
        {
            'name': 'Accounts',
            'link': False,
        }
    ]

    def get(self, request):
        context = {
            'title': self.page_title,
            'breadcrumbs': self.breadcrumbs,
        }

        return render(request, self.template_name, context)


class AccountDetailsView(TekyyView):
    template_name = 'pages/accounts/single-account/index.html'
    page_title = 'View Account'
    breadcrumbs = [
        {
            'name': 'Accounts',
            'link': '/accounts/',
        },
        {
            'name': '1',
            'link': False
        }
    ]

    def get(self, request, account_id):
        context = {
            'title': self.page_title,
            'breadcrumbs': self.breadcrumbs,
            'account_id': account_id,
        }

        return render(request, self.template_name, context)
