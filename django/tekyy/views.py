from django.shortcuts import render
from django.views.generic import TemplateView

# Create your views here.


class TekyyView(TemplateView):
    template_name = 'pages/dashboard.html'

    def get(self, request):
        return render(request, self.template_name)
